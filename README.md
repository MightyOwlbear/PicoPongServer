# PicoPongServer

Scripts to accompany the "Serve HTML games over your local network" tutorial in The MagPi.

Server script based on [Nathen Busler's Pico W LED web server](https://github.com/pi3g/pico-w/tree/main/MicroPython/I%20Pico%20W%20LED%20web%20server) (MIT license), [Basic Pong HTML and JavaScript Game](https://gist.github.com/straker/81b59eecf70da93af396f963596dfdc5/) game by Straker (licensed CC0 1.0 Universal).